import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
require('./codebase/dhtmlxgantt')
require('./codebase/ext/dhtmlxgantt_grouping')
require('./codebase/locale/locale_cn')
require('./codebase/ext/dhtmlxgantt_smart_rendering')
require('./codebase/ext/dhtmlxgantt_undo')
require('./codebase/ext/dhtmlxgantt_fullscreen')
require('./codebase/ext/dhtmlxgantt_tooltip')
require('./scripts/zoom_to_fit')
require('./scripts/zoom')
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
