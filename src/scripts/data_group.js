import datasource from '../views/datasrouce'
import _ from 'lodash'

const cache = {
  timelines: [], //事件序列
  ownerMap: {}, // 按照用户分组
  ownerValues: [], // 二维数组
  owners: [], // 用户列表
  target: {
    groups: []
  },
  root: {
    groups: []
  },
  index: 0
}


/**
 * 解析日期字符串为时间戳
 * @param dateStr
 * @returns {{timestamp: number}}
 */
const setTimestamp = (dateStr) => {
  return {timestamp: Date.parse(dateStr.replace(/-/g, '/'))}
}

/**
 * 按照时间排序
 * 把原始数据拆分，添加时间戳属性，节点名称属性
 * @param list
 */
const sortWithTimes = (list) => {
  //拆分区间事件
  list.forEach((data) => {
    if (data.type === 'task') {
      cache.timelines.push(Object.assign({}, data, setTimestamp(data.start_date), {
        id: data.id + '#start',
        name: data.event_type + '#start'
      }))
      cache.timelines.push(Object.assign({}, data, setTimestamp(data.end_date), {
        id: data.id + '#end',
        name: data.event_type + '#end'
      }))
    } else {
      cache.timelines.push(Object.assign({}, data, setTimestamp(data.start_date), {name: data.event_type}))
    }
  })
  // 按照时间戳进行排序 升序排序
  cache.timelines.sort((a, b) => {
    return a.timestamp - b.timestamp;
  })
  console.log('【timelines】 \n %o', cache.timelines)
}

/**
 * 按照属性进行分组
 */
const groupByOwnerId = () => {
  cache.ownerMap = _.groupBy(cache.timelines, 'owner_id')
  console.log('【ownerGroups】 \n %o', cache.ownerMap)
  // console.log('【ownerGroups JSONStringify】 \n %o', JSON.stringify(cache.ownerMap))
}

const buildTemps = () => {
  cache.ownerValues = Object.values(cache.ownerMap) // 二维数组
  cache.ownerList = Object.keys(cache.ownerMap) // 用户列表
  cache.ownerCount = cache.ownerList.length // 用户列表
}

/**
 * 聚合数据，生成组
 */
const splitRow = (groupsRow, index, context, isNewGroup) => {
  const tempList = []
  Object.values(cache.ownerMap).forEach((value) => {
    if (value.length > index) {
      tempList.push(value[index])
    }
  })
  // 按照名字分组 count=4 {'中风': [{owner_id:'01'},{owner_id:'02'},{owner_id:'03'}], '药物A':[{owner_id:'11', {owner_id:'12'}}]}
  const nameMap = _.groupBy(tempList, 'name');
  const nameValues = Object.values(nameMap).sort((a, b) => a.length - b.length) // 二维数组，长度长的在前面 名字分组的值
  nameValues.forEach((arr) => {
    if (arr.length > 1) { // 可聚合
      if (cache.ownerCount === arr.length) { // 都聚合成一个
        if (context.parent) { // 如果父节点存在

        }
        const row = groupsRow || [] // 聚合数组是否存在，不存在新建一个
        const event = Object.assign(arr[0], {
          count: arr.length,
          name: arr[0].name,
          // parent: context,
          list: arr.map((data) => data.id)
        })
        row.push(event); // 聚合元素添加到聚合数组里
        if (!groupsRow) { // 如果聚合数组不存在，则添加到父的聚合数组里
          context.groups.unshift(row)
        }
        index++
        splitRow(row, index, event)
      } else { // 聚合成多行
        const row = []
        const event = Object.assign(arr[0], {
          count: arr.length,
          // parent: context,
          list: arr.map((data) => data.id),
          groups: []
        })
        row.push(event)
        context.groups.unshift(row)
        index++
        splitRow(row, index, event)
      }
    } else { // 不可分组
      cache.ownerCount--
      const list = cache.ownerMap[arr[0].owner_id].slice(index)
      const eventList = []
      list.forEach((data) => {
        eventList.push(Object.assign(data, {
          count: 1,
          // parent: context,
          list: [data.id]
        }))
      })
      delete cache.ownerMap[arr[0].owner_id]
      if (!context.groups) { // 子聚合数组
        context.groups = []
        groupsRow = context.groups
      }
      context.groups.push(eventList)
    }
  })

}

const generateSub = (context, subOwnerMap, curlist) => {
  const tempList = []
  Object.values(subOwnerMap).forEach((value) => {
    tempList.push(value[0])
  })
  const nameMap = _.groupBy(tempList, 'name');
  const nameValuesArray = Object.values(nameMap).sort((a, b) => b.length - a.length)
  nameValuesArray.forEach((arr) => {
    if (arr.length > 1) { // 多个聚合成一个
      const point = arr[0]
      const forList = curlist || []
      const node = {
        text: point.text,
        event_type: point.event_type,
        count: 1,
        name: point.name,
        ownerList: arr.map(point => point.owner_id),
        nodeList: arr.map(point => point.id),
        parent: context,
        forList: forList,
        groups: []
      }
      if(curlist){
        curlist.push(node)
      }else {
        forList.push(node)
        context.groups.unshift(forList)
      }
      const _subOwnerMap = {}
      node.ownerList.forEach((owner_id) => {
        if (subOwnerMap[owner_id].length > 1) {
          _subOwnerMap[owner_id] = subOwnerMap[owner_id].slice(1)
        }
      })
      const root = cache.root
      generateSub(node, _subOwnerMap, forList)
    } else { // 自己聚合成一个
      const list = []
      subOwnerMap[arr[0].owner_id].forEach((point) => {
        const node = {
          text: point.text,
          event_type: point.event_type,
          count: 1,
          name: point.name,
          ownerList: [point.owner_id],
          nodeList: [point.id],
          parent: context,
          forList: list
        }
        list.push(node)
      })
      context.groups.push(list)
    }
  })
}

const generateRoot = () => {
  const tempList = []
  Object.values(cache.ownerMap).forEach((value) => {
    tempList.push(value[0])
  })
  // 按照名字分组 count=4 {'中风': [{owner_id:'01'},{owner_id:'02'},{owner_id:'03'}], '药物A':[{owner_id:'11', {owner_id:'12'}}]}
  const nameMap = _.groupBy(tempList, 'name');
  const nameValues = Object.values(nameMap).sort((a, b) => a.length - b.length)
  nameValues.forEach((value) => {
    if (value.length > 1) { // 可进行分组
      const rootNode = {
        count: value.length,
        text: value[0].text,
        event_type: value[0].event_type,
        name: value[0].name,
        ownerList: value.map((val) => {
          return val.owner_id
        }),
        nodeList: value.map((val) => {
          return val.id
        }),
        parent: cache.root,
        groups: []
      }
      const list = []
      list.push(rootNode)
      rootNode.forList = list
      cache.root.groups.unshift([rootNode])
      const subOwnerMap = {}
      rootNode.ownerList.forEach((owner_id) => {
        if (cache.ownerMap[owner_id].length > 1) {
          subOwnerMap[owner_id] = cache.ownerMap[owner_id].slice(1)
        }
      })
      generateSub(rootNode, subOwnerMap)
    } else { // 不可进行分组
      const list = []
      cache.ownerMap[value[0].owner_id].forEach((point) => {
        const node = {
          text: point.text,
          event_type: point.event_type,
          count: 1,
          name: point.name,
          ownerList: [point.owner_id],
          nodeList: [point.id],
          parent: cache.root,
          forList: list
        }
        list.push(node)
      })
      cache.root.groups.push(list)
    }
  })
  console.log('[cache.root] \n %o', cache.root)
}

export const group = (list) => {
  sortWithTimes(list)
  groupByOwnerId()
  buildTemps()
  generateRoot()
  // splitRow(null, 0, cache.target, true)
  // console.log('【target】\n %o', cache.target)
  // console.log('【target stringify】\n %o', JSON.stringify(cache.target))
}

group(datasource.data)

export default group