import datasource from '../datasrouce'
import _ from 'lodash'

export default {
  countTotal: 6,
  durationTotal: 300,
  // minus: {
  //   groups: [
  //     [
  //       {
  //         id: '001',
  //         duration: 0,
  //         count: 3,
  //         color: '#108950',
  //         type: 'milestone',
  //         event_type: 'milestone_001',
  //         range_type: '',
  //         begin: {id: '001'},
  //         end: {id: '001'},
  //         groups: [
  //           [
  //             {
  //               id: '010',
  //               duration: 10,
  //               count: 1,
  //               color: '#3fd6cc',
  //               type: 'task',
  //               event_type: 'task_010',
  //               begin: {id: '010'},
  //               end: {id: '010'},
  //             }
  //           ],
  //           [
  //             {
  //               id: '010',
  //               duration: 20,
  //               count: 2,
  //               color: '#d6c28a',
  //               type: 'task',
  //               event_type: 'task_010',
  //               begin: {id: '010'},
  //               end: {id: '010'},
  //             },
  //             {
  //               id: '010',
  //               duration: 40,
  //               count: 2,
  //               color: '#654c89',
  //               type: 'task',
  //               event_type: 'task_010',
  //               begin: {id: '010'},
  //               end: {id: '010'},
  //               list: [
  //                 [
  //                   {
  //                     id: '010',
  //                     duration: 20,
  //                     count: 1,
  //                     color: '#d6d51f',
  //                     type: 'task',
  //                     event_type: 'task_010',
  //                     begin: {id: '010'},
  //                     end: {id: '010'},
  //                   }
  //                 ],
  //                 [
  //                   {
  //                     id: '010',
  //                     duration: 20,
  //                     count: 1,
  //                     color: '#d64922',
  //                     type: 'task',
  //                     event_type: 'task_010',
  //                     begin: {id: '010'},
  //                     end: {id: '010'},
  //                   }
  //                 ]
  //               ]
  //             }
  //           ]
  //         ]
  //       }
  //     ],
  //     [
  //       {
  //         id: '010',
  //         duration: 30,
  //         count: 2,
  //         color: '#89162d',
  //         type: 'task',
  //         event_type: 'task_010',
  //         begin: {id: '010'},
  //         end: {id: '010'},
  //       },
  //       {
  //         id: '010',
  //         duration: 60,
  //         count: 2,
  //         color: '#818919',
  //         type: 'blank',
  //         event_type: 'task_010',
  //         begin: {id: '010'},
  //         end: {id: '010'},
  //       },
  //       {
  //         id: '010',
  //         duration: 60,
  //         count: 2,
  //         color: '#89217f',
  //         type: 'task',
  //         event_type: 'task_010',
  //         begin: {id: '010'},
  //         end: {id: '010'},
  //       }
  //     ],
  //     [
  //       {
  //         id: '010',
  //         duration: 30,
  //         count: 2,
  //         color: '#373d89',
  //         type: 'task',
  //         event_type: 'task_010',
  //         begin: {id: '010'},
  //         end: {id: '010'},
  //       }
  //     ]
  //   ]
  // },
  plus: {
    groups: [
      [
        {
          duration: 10,
          count: 3,
          owners: ['id'],
          start: {
            color: '#1d9806'
          },
          end: {
            color: '#1d9806'
          }
        },
        {
          id: '001',
          duration: 0,
          count: 3,
          start: {
            color: '#98450e'
          },
          end: {
            color: '#98450e'
          },
          type: 'milestone',
          event_type: 'milestone_001',
          list: ['', '', ''],
          groups: [
            [
              {
                id: '010',
                duration: 10,
                count: 1,
                start: {
                  color: '#980c7e'
                },
                end: {
                  color: '#980c7e'
                },
                type: 'task',
                event_type: 'task_010',
                begin: {id: '010'},
              }
            ],
            [
              {
                id: '010',
                duration: 20,
                count: 2,
                start: {
                  color: '#138598'
                },
                end: {
                  color: '#980c47'
                },
                type: 'task',
                event_type: 'task_010',
                begin: {id: '010'},
              },
              {
                id: '010',
                duration: 40,
                count: 2,
                start: {
                  color: '#980c47'
                },
                end: {
                  color: '#3a981c'
                },
                type: 'task',
                event_type: 'task_010',
                begin: {id: '010'},
                groups: [
                  [
                    {
                      id: '010',
                      duration: 20,
                      count: 1,
                      start: {
                        color: '#989511'
                      },
                      end: {
                        color: '#989511'
                      },
                      type: 'task',
                      event_type: 'task_010',
                    }
                  ],
                  [
                    {
                      id: '010',
                      duration: 20,
                      count: 1,
                      start: {
                        color: '#249870'
                      },
                      end: {
                        color: '#249870'
                      },
                      type: 'task',
                      event_type: 'task_010',
                      begin: {id: '010'},
                    }
                  ]
                ]
              }
            ]
          ]
        }
      ],
      // [
      //   {
      //     id: '010',
      //     duration: 30,
      //     count: 2,
      //     color: '#89162d',
      //     type: 'task',
      //     event_type: 'task_010',
      //     begin: {id: '010'},
      //     end: {id: '010'},
      //   },
      //   {
      //     id: '010',
      //     duration: 60,
      //     count: 2,
      //     color: '#818919',
      //     type: 'blank',
      //     event_type: 'task_010',
      //     begin: {id: '010'},
      //     end: {id: '010'},
      //   },
      //   {
      //     id: '010',
      //     duration: 60,
      //     count: 2,
      //     color: '#818919',
      //     type: 'task',
      //     event_type: 'task_010',
      //     begin: {id: '010'},
      //     end: {id: '010'},
      //   }
      // ],
      // [
      //   {
      //     id: '010',
      //     duration: 30,
      //     count: 1,
      //     color: '#373d89',
      //     type: 'task',
      //     event_type: 'task_010',
      //     begin: {id: '010'},
      //     end: {id: '010'},
      //   }
      // ]
    ]
  },
}


/**
 * 按照人进行分组
 * @param dataSource
 */
const getOwnerGroup = (dataSource) => {
  return _.groupBy(dataSource, (data) => {
    return data.owner_id
  })
}
/**
 * 按照事件分组
 * @param dataSource
 */
const getEventGroup = (dataSource) => {
  return _.groupBy(dataSource, (data) => {
    return data.event_type
  })
}

const setTimestamp = (dateStr) => {
  return {timestamp: Date.parse(dateStr.replace(/-/g, '/'))}
}

const middleData = {}
let index = 0;
let timelines = [] // 把任务拆分成开始节点和结束节点，并且按照时间进行排序


const generateGroups = (ownerGroups, target) => {
  let tempOwnerGroupValues = Object.values(ownerGroups)
  let groups = []
  const array = []
  for (let i = 0; i < tempOwnerGroupValues.length; i++) {
    const ownerList = tempOwnerGroupValues[i]
    array.push(ownerList[0])
  }
  let groupName = _.groupBy(array, 'name')
  const groupNameValues = Object.values(groupName)
  console.log('groupNameValues = %o', groupNameValues)
  groupNameValues.sort((a, b) => {
    return a.length - b.length
  }).forEach((arr) => {
    if (arr.length > 1) { //有分组
      const subGroups = []
      const tempGroup = {}
      arr.forEach((event) => {
        const ownerValues = ownerGroups[event.owner_id]
        if (ownerValues.length > 1) {
          const arr = ownerValues.slice(1)
          subGroups.push(arr)
          tempGroup[event.owner_id] = arr
        }
      })
      if (subGroups.length > 2) { // 还可进行分组
        console.warn('还可进行分组')
        const obj = arr[0]
        groups.unshift([obj])
        target.groups = groups
        // generateGroups(tempGroup, obj);
      } else { // 不可进行分组了
        const obj = arr[0]
        obj.groups = subGroups
        groups.unshift([obj])
        console.warn('不可进行分组了')
      }

    } else { // 一个用户自己一组
      console.warn('一个用户自己一组')
      const ownerValues = ownerGroups[arr[0].owner_id]
      groups.unshift(ownerValues)
    }
  })
  target.groups = groups
}

const asyncGroup = (dataSource) => {
  const dataList = dataSource.data;
  dataList.forEach((data) => {
    if (data.type === 'task') {
      timelines.push(Object.assign({}, data, setTimestamp(data.start_date), {
        id: data.id + '#start',
        name: data.event_type + '#start'
      }))
      timelines.push(Object.assign({}, data, setTimestamp(data.end_date), {
        id: data.id + '#end',
        name: data.event_type + '#start'
      }))
    } else {
      timelines.push(Object.assign({}, data, setTimestamp(data.start_date), {name: data.event_type}))
    }
  })
  // 按照时间戳进行排序
  timelines.sort((a, b) => {
    return a.timestamp - b.timestamp;
  })
  console.log('timelines %o', timelines)
  // 按照人进行分组
  let ownerGroups = getOwnerGroup(timelines)
  console.log('ownerGroups %o', ownerGroups)
  let ownerGroupValues = Object.values(ownerGroups)
  ownerGroupValues.sort((a, b) => {
    return b.length - a.length;
  })
  console.log('ownerGroupValues => %o', ownerGroupValues)
  generateGroups(ownerGroups, middleData)
  console.log('分组中间态 %o', middleData)
  console.log(JSON.stringify(middleData))
}
// asyncGroup(datasource)


export {asyncGroup}