export default {
  data: [
    {
      id: 'id_01',
      text: '中风',
      start_date: '2017-04-14 00:00:00',
      type: 'milestone',
      duration: 0,
      event_type: 'milestone_01',
      owner_id: "1"
    },
    {
      id: 'id_02',
      text: '药物A',
      start_date: '2017-04-15 12:30:00',
      type: 'task',
      duration: 28,
      end_date: '2017-04-16 16:30:00',
      event_type: 'event_02',
      owner_id: "1"
    },
    {
      id: 'id_03',
      text: '药物B',
      start_date: '2017-04-18 00:00:00',
      duration: 96,
      end_date: '2017-04-22 00:00:00',
      type: 'task',
      event_type: 'event_03',
      owner_id: "1"
    },
    {
      id: 'id_11',
      text: '中风',
      start_date: '2017-04-14 00:00:00',
      type: 'milestone',
      duration: 0,
      event_type: 'milestone_01',
      owner_id: "11"
    },
    {
      id: 'id_12',
      text: '药物A',
      start_date: '2017-04-15 12:30:00',
      type: 'task',
      duration: 28,
      end_date: '2017-04-16 16:30:00',
      event_type: 'event_02',
      owner_id: "11"
    },
    // {
    //   id: 'id_13',
    //   text: '药物B',
    //   start_date: '2017-04-18 00:00:00',
    //   duration: 96,
    //   end_date: '2017-04-22 00:00:00',
    //   type: 'task',
    //   event_type: 'event_03',
    //   owner_id: "11"
    // },
    {
      id: 'id_05',
      text: '中风',
      start_date: '2017-04-15 00:00:00',
      type: 'milestone',
      duration: 0,
      event_type: 'milestone_01',
      owner_id: "2"
    },
    {
      id: 'id_06',
      text: '药物B',
      start_date: '2017-04-15 00:00:00',
      end_date: '2017-04-15 08:00:00',
      duration: 8,
      type: 'task',
      event_type: 'event_03',
      owner_id: "2"
    },
    // {id: 'id_07', text: '中风', start_date: '2017-04-14 00:00:00', type: 'milestone', event_type: 'milestone_01', owner_id: "3"},
    {
      id: 'id_08',
      text: '药物C',
      start_date: '2017-04-15 00:00:00',
      end_date: '2017-04-15 03:00:00',
      type: 'task',
      duration: 3,
      progress: 1,
      event_type: 'event_04',
      owner_id: "3"
    },
    {
      id: 'id_09',
      text: '药物D',
      start_date: '2017-04-15 12:30:00',
      end_date: '2017-04-15 16:00:00',
      type: 'task',
      duration: 3.5,
      progress: 1,
      event_type: 'event_05',
      owner_id: "3"
    }
  ]
}