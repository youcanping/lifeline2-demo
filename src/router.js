import Vue from "vue";
import Router from "vue-router";
import GanttView from "./views/GanttView.vue";
import Home from "./views/Home.vue";
import GroupView from "./views/GroupView/GroupView.vue";
import NewGroupView from './views/GroupView/NewGroupView.vue'
Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/gantt",
      name: "gantt",
      component: GanttView
    },
    {
      path: '/group',
      name: 'group',
      component: GroupView
    },
    {
      path: '/new',
      name: 'new',
      component: NewGroupView
    }
  ]
});
